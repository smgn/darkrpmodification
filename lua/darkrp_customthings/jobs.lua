--[[---------------------------------------------------------------------------
DarkRP custom jobs
---------------------------------------------------------------------------

This file contains your custom jobs.
This file should also contain jobs from DarkRP that you edited.

Note: If you want to edit a default DarkRP job, first disable it in darkrp_config/disabled_defaults.lua
	Once you've done that, copy and paste the job to this file and edit it.

The default jobs can be found here:
https://github.com/FPtje/DarkRP/blob/master/gamemode/config/jobrelated.lua

For examples and explanation please visit this wiki page:
http://wiki.darkrp.com/index.php/DarkRP:CustomJobFields


Add jobs under the following line:
---------------------------------------------------------------------------]]

TEAM_CLONE_CADET = DarkRP.createJob("Clone Cadet", {
    color = Color(0, 119, 40, 255),
    model = {"models/npc/player/asgclonewars/clone_cadet/clone_cadet.mdl"},
    description = [[]],
    weapons = {"keys",},
    command = "cc",
    max = 0,
    salary = GAMEMODE.Config.normalsalary,
    admin = 0,
    vote = false,
    hasLicense = false,
    candemote = false,
    category = "Clone trooper",
})

TEAM_TSC = DarkRP.createJob("Talon Squad Commander", {
   color = Color(0, 178, 82, 255),
   model = {"models/player/asgclonewars/talon_squad/talon_officer.mdl"},
   description = [[]],
   weapons = {"weapon_lasermgun", "dc17grapple", "weapon_752_dc17dual", "weapon_swrc_det", "weapon_752_dc17m_sn", "keys", "realrbn_tazer_mr", "weapon_752_dc15sa", },
   command = "tsc",
   max = 1,
   salary = 500,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Talon Squad",
})

TEAM_TS = DarkRP.createJob("Talon Squad Trooper", {
   color = Color(0, 140, 64, 255),
   model = {"models/player/asgclonewars/talon_squad/talon_trooper.mdl"},
   description = [[]],
   weapons = {"weapon_752_dc15a", "realrbn_tazer_mr", "weapon_752bf3_binoculars", "keys", "weapon_752_dc15sa", },
   command = "/ts",
   max = 9,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = false,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Talon Squad",
})

TEAM_CP = DarkRP.createJob("Youngling", {
   color = Color(255, 255, 255, 255),
   model = {"models/jazzmcfly/jka/younglings/jka_young_male.mdl"},
   description = [[]],
   weapons = {"keys", "weapon_lightsaber"},
   command = "/cp",
   max = 6,
   salary = 0,
   admin = 1,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Jedi", 
})

TEAM_CFOX = DarkRP.createJob("Commander Fox ", {
   color = Color(181, 0, 0, 255),
   model = {"models/player/fox/ccfox.mdl"},
   description = [[]],
   weapons = {"weapon_752_dc15a", "weapon_752_dc15s", "weapon_752_dc17", "dc17grapple", "keys", "areststick", "unarreststick", "stunstick", },
   command = "/cf",
   max = 1,
   salary = 250,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Shock",
})

TEAM_SHOCK = DarkRP.createJob("Shock Trooper", {
   color = Color(255, 0, 107, 255),
   model = {"models/sgg/starwars/clonetrooper_thire.mdl"},
   description = [[]],
   weapons = {"arreststick", "unarreststick", "stunstick", "weapon_752_dc15sa", "dc_15a", "realrbn_tazer_mr", "keys", },
   command = "/st",
   max = 9,
   salary = 100,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Shock",
})

TEAM_CT = DarkRP.createJob("Clone trooper", {
   color = Color(188, 188, 188, 255),
   model = {"models/sgg/starwars/clonetrooper_clean.mdl"},
   description = [[]],
   weapons = {"dc15a", "dc15s", "keys",},
   command = "/ct",
   max = 1000,
   salary = 50,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Clone trooper",
})

TEAM_JP = DarkRP.createJob("Jedi Padawan", {
   color = Color(0, 239, 255, 255),
   model = {"models/grealms/characters/jedibattlelord/jedibattlelord.mdl"},
   description = [[]],
   weapons = {"weapon_lightsaber", "keys", "swep_jedi_hands" },
   command = "/jp",
   max = 1000,
   salary = 200,
   admin = 0,
   vote = false,
   hasLicense = true,
   candemote = false,
   -- CustomCheck
   medic = false,
   chief = false,
   mayor = false,
   hobo = false,
   cook = false,
   category = "Jedi",
})
GAMEMODE.DefaultTeam = TEAM_CLONE_CADET

--GAMEMODE.DefaultTeam = TEAM_CITIZEN
